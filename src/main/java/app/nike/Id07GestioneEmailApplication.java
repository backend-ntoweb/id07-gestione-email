package app.nike;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableEurekaClient
@EnableFeignClients
@SpringBootApplication
public class Id07GestioneEmailApplication {

	public static void main(String[] args) {
		SpringApplication.run(Id07GestioneEmailApplication.class, args);
	}

}
