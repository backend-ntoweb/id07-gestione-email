package app.nike.service;

import org.springframework.mail.javamail.JavaMailSender;

import app.nike.models.Email;


public interface EmailSender {

	public JavaMailSender getJavaMailSender();
	
	public void inviaEmail(Email email);

}
