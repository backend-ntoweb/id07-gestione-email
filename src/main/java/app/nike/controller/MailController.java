package app.nike.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.nike.models.Email;
import app.nike.service.EmailSender;

@RestController
@RequestMapping("email")
public class MailController {
	@Autowired EmailSender emailSender;

	@PostMapping("/invia")
	public void inviaEmail(@Valid @RequestBody Email emailDaInviare) {
		emailSender.inviaEmail(emailDaInviare);
	}
}
